/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
#-- getFile(ID);
#-- 
#-- 
#-- FileID,
#-- FilePath,
#-- FileSize,
#-- FileHash,
#-- FileDescription,
#-- FileCreated,
#-- FileCreator,
#-- FileGenerator,
#-- FileOwner,
#-- FilePermission,
#-- FileChecksum

Drop Procedure if exists db_2_0.getFile;
Delimiter //
Create Procedure db_2_0.getFile (in FileID INT)
BEGIN 

Select name, description, role into @name, @description, @role from entities where id=FileID LIMIT 1;

IF @role = 'file' Then
		Select path, hash, size into @FilePath, @FileHash, @FileSize from files where file_id=FileID LIMIT 1;
		Select timestamp, user_id, user_agent into @FileCreated, @FileCreator, @FileGenerator from history where entity_id=FileID AND event='insertion' LIMIT 1;

Select 
FileID as FileID,
@FilePath as FilePath,
@FileSize as FileSize,
@FileHash as FileHash,
@FileDescription as FileDescription,
@FileCreated as FileCreated,
@FileCreator as FileCreator,
@FileGenerator as FileGenerator,
NULL	as FileOwner,
NULL as FilePermission,
NULL as FileChecksum;

END IF;

END;
//
delimiter ;
