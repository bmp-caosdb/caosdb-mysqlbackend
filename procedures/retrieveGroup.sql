/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

Drop Procedure if exists db_2_0.retrieveGroup;
Delimiter //
Create Procedure db_2_0.retrieveGroup(in USERID INT UNSIGNED, in GROUPID INT UNSIGNED)


BEGIN 
IF USERID IS NOT NULL THEN 
  Select user_id, group_id from groups where user_id = USERID;
ELSEIF GROUPID IS NOT NULL THEN
  Select user_id, group_id from groups where group_id = GROUPID;
END IF;

END;
//
delimiter ;
