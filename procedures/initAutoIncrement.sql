/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP PROCEDURE IF EXISTS db_2_0.initAutoIncrement;
delimiter //

CREATE PROCEDURE db_2_0.initAutoIncrement()
BEGIN

    SELECT @max := MAX(entity_id)+ 1 FROM transaction_log; 
    IF @max IS NOT NULL THEN
        SET @stmtStr = CONCAT('ALTER TABLE entities AUTO_INCREMENT=',@max);
        PREPARE stmt FROM @stmtStr;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
    END IF;

END;
//
delimiter ;
