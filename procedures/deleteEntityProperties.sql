/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP PROCEDURE IF EXISTS db_2_0.deleteEntityProperties;
delimiter //

CREATE PROCEDURE db_2_0.deleteEntityProperties(in EntityID INT UNSIGNED)
BEGIN

CALL deleteIsa(EntityID);

DELETE FROM reference_data 
where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM null_data 
where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM text_data 
where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM name_data 
where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM enum_data 
where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM integer_data 
where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM double_data 
where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM datetime_data 
where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM date_data 
where (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM name_overrides
WHERE (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM desc_overrides
WHERE (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID;
DELETE FROM data_type 
WHERE (domain_id=0 AND entity_id=EntityID) OR domain_id=EntityID OR (domain_id=0 AND entity_id=0 AND property_id=EntityID);

DELETE FROM query_template_def WHERE id=EntityID;

END;
//
delimiter ;
