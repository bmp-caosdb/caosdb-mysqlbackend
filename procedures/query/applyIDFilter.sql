/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP PROCEDURE IF EXISTS db_2_0.applyIDFilter;

DELIMITER //

CREATE PROCEDURE db_2_0.applyIDFilter(in sourceSet VARCHAR(255), in targetSet VARCHAR(255), in o CHAR(2), in vInt BIGINT, in agg CHAR(3))
IDFILTER_LABEL: BEGIN
DECLARE data VARCHAR(20000) DEFAULT NULL;
DECLARE aggVal VARCHAR(255) DEFAULT NULL;

#-- get aggVal if possible
IF agg IS NOT NULL THEN
	SET @stmtIDAggValStr = CONCAT("SELECT ", agg, "(id) INTO @sAggVal FROM `", sourceSet, "`");
	PREPARE stmtIDAggVal FROM @stmtIDAggValStr;
	EXECUTE stmtIDAggVal;
    DEALLOCATE PREPARE stmtIDAggVal;
    SET aggVal = @sAggVal;
END IF;

#-- generate stmt string
IF targetSet IS NULL OR targetSet = sourceSet THEN
    SET data = CONCAT("DELETE FROM `",sourceSet,"` WHERE ",IF(o IS NULL OR vInt IS NULL,"1=1",CONCAT("NOT id",o,vInt)),IF(aggVal IS NULL, "", CONCAT(" AND id!=",aggVal)));
ELSE
    SET data = CONCAT("INSERT IGNORE INTO `",targetSet,"` SELECT data.id as id FROM `",sourceSet,"` AS data WHERE ",IF(o IS NULL OR vInt IS NULL,"1=1",CONCAT("data.id",o,vInt)),IF(aggVal IS NULL, "", CONCAT(" AND data.id=", aggVal)));
END IF;

Set @stmtIDFilterStr = data;
PREPARE stmtIDFilter FROM @stmtIDFilterStr;
EXECUTE stmtIDFilter;
DEALLOCATE PREPARE stmtIDFilter;
	
END;
//

DELIMITER ;
