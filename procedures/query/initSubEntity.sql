/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */


DROP PROCEDURE IF EXISTS db_2_0.initSubEntity;

DELIMITER //

CREATE PROCEDURE db_2_0.initSubEntity(in e_id INT UNSIGNED, in ename VARCHAR(255), in tableName VARCHAR(255))
BEGIN
	DECLARE ecount INT DEFAULT 0;
   
    SET @stmtStr = CONCAT('INSERT IGNORE INTO `', tableName, '` (id) SELECT id FROM entities WHERE name = ? UNION ALL SELECT entity_id FROM name_data WHERE value=? AND domain_id=0;');
    PREPARE stmt FROM @stmtStr;
	SET @ename = ename;
    EXECUTE stmt USING @ename, @ename;
    SET ecount = ROW_COUNT();
	DEALLOCATE PREPARE stmt;

    IF e_id IS NOT NULL THEN
        SET @stmtStr = CONCAT('INSERT IGNORE INTO `', tableName, '` (id) VALUES (', e_id, ')');
        PREPARE stmt FROM @stmtStr;
        EXECUTE stmt;
        SET ecount = ecount + ROW_COUNT();
		DEALLOCATE PREPARE stmt;
    END IF;

    IF ecount > 0 THEN
        call getChildren(tableName);
    END IF;
    
END;
// 
DELIMITER ;
