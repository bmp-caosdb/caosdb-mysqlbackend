/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

Drop Procedure if exists db_2_0.getChildren;
Delimiter //
Create Procedure db_2_0.getChildren(in tableName varchar(255))

BEGIN

    CREATE TEMPORARY TABLE dependTemp (id INT UNSIGNED PRIMARY KEY);



    SET @initDepend = CONCAT('INSERT IGNORE INTO dependTemp (id) SELECT i.child FROM isa_cache AS i INNER JOIN `', tableName, '` AS t ON (i.parent=t.id);');
    PREPARE initDependStmt FROM @initDepend;

	EXECUTE initDependStmt;
	IF ROW_COUNT() != 0 THEN
    	SET @transfer = CONCAT('INSERT IGNORE INTO `', tableName, '` (id) SELECT id FROM dependTemp');
        PREPARE transferstmt FROM @transfer;
		EXECUTE transferstmt;
		DEALLOCATE PREPARE transferstmt;
	END IF;


	DEALLOCATE PREPARE initDependStmt;
	DROP TEMPORARY TABLE dependTemp;

END;
//
delimiter ;
