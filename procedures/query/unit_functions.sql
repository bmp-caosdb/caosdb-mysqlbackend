/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
DELIMITER //

DROP FUNCTION IF EXISTS db_2_0.convert_unit//

CREATE FUNCTION db_2_0.convert_unit(unit_sig BIGINT, value DECIMAL(65,30)) RETURNS DECIMAL(65,30) DETERMINISTIC
#-- returns NULL IFF unit_sig IS NULL;
BEGIN
    DECLARE ret DECIMAL(65,30) DEFAULT value;

    SELECT (((value+a)*b_dividend)/b_divisor+c) INTO ret FROM units_lin_con WHERE signature_from=unit_sig;
    RETURN ret;
END;
//

DROP FUNCTION IF EXISTS db_2_0.standard_unit//

CREATE FUNCTION db_2_0.standard_unit(unit_sig BIGINT) RETURNS BIGINT DETERMINISTIC
#-- returns NULL IFF the unit cannot be converted to any other unit (especially because it is a base unit or yet in its standard form).
BEGIN
    DECLARE ret BIGINT DEFAULT unit_sig;

    SELECT signature_to INTO ret FROM units_lin_con WHERE signature_from=unit_sig;
    RETURN ret;
END;
//

DELIMITER ;
