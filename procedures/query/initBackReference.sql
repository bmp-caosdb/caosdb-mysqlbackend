/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP PROCEDURE IF EXISTS db_2_0.initBackReference;
DELIMITER //
CREATE PROCEDURE db_2_0.initBackReference(in pid INT UNSIGNED, in pname VARCHAR(255), in entity_id INT UNSIGNED, in ename VARCHAR(255))
BEGIN
	DECLARE propertiesTable VARCHAR(255) DEFAULT NULL;
	DECLARE entitiesTable VARCHAR(255) DEFAULT NULL;

    IF pname IS NOT NULL THEN
        call createTmpTable(propertiesTable);
        call initSubEntity(pid, pname, propertiesTable);
    END IF;

    IF ename IS NOT NULL THEN
        call createTmpTable(entitiesTable);
        call initSubEntity(entity_id, ename, entitiesTable);
    END IF;

	SELECT propertiesTable, entitiesTable;


END //
DELIMITER ;
