/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */



DROP PROCEDURE IF EXISTS db_2_0.initEmptyTargetSet;
DELIMITER //

CREATE PROCEDURE db_2_0.initEmptyTargetSet(in targetSet VARCHAR(255))
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT targetSet;
    IF targetSet IS NOT NULL THEN
        SET @isNotEmptyVar = NULL; /*NULL means targetSet is still empty*/
        SET @isEmptyStmtStr = CONCAT("SELECT 1 INTO @isNotEmptyVar FROM `",targetSet,"` LIMIT 1");
        PREPARE stmtIsNotEmpty FROM @isEmptyStmtStr;
        EXECUTE stmtIsNotEmpty;
        DEALLOCATE PREPARE stmtIsNotEmpty;
        IF @isNotEmptyVar IS NOT NULL THEN /*if targetSet is not empty*/
            call createTmpTable(newTableName);
        END IF;
    ELSE
        call createTmpTable(newTableName);
    END IF;
    SELECT newTableName AS newTableName;
END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS db_2_0.initDisjunctionFilter;
DELIMITER //

CREATE PROCEDURE db_2_0.initDisjunctionFilter()
BEGIN
    call initEmptyTargetSet(NULL);
END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS db_2_0.initNegationFilter;
DELIMITER //

CREATE PROCEDURE db_2_0.initNegationFilter(in sourceSet VARCHAR(255))
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT NULL;
    call createTmpTable(newTableName);
    call copyTable(sourceSet, newTableName);
    SELECT newTableName AS newTableName;
END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS db_2_0.initConjunctionFilter;
DELIMITER //

CREATE PROCEDURE db_2_0.initConjunctionFilter(in sourceSet VARCHAR(255))
BEGIN
    DECLARE newTableName VARCHAR(255) DEFAULT NULL;
    call createTmpTable(newTableName);
    call copyTable(sourceSet, newTableName);
    SELECT newTableName AS newTableName;
END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS db_2_0.finishNegationFilter;
DELIMITER //

CREATE PROCEDURE db_2_0.finishNegationFilter(in sourceSet VARCHAR(255), in targetSet VARCHAR(255), in subResultSet VARCHAR(255))
BEGIN
    IF targetSet IS NULL OR sourceSet = targetSet THEN
        call calcDifference(sourceSet, subResultSet);
    ELSE
        call calcComplementUnion(targetSet,subResultSet,sourceSet);
    END IF;
END;
// 
DELIMITER ;
