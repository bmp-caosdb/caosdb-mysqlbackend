/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
DROP PROCEDURE IF EXISTS db_2_0.applyTransactionFilter;
DELIMITER //

CREATE PROCEDURE db_2_0.applyTransactionFilter(in sourceSet VARCHAR(255), targetSet VARCHAR(255), in transaction VARCHAR(255), in operator_u CHAR(2), in realm VARCHAR(255), in userName VARCHAR(255), in ilb BIGINT, in ilb_nanos INT UNSIGNED, in eub BIGINT, in eub_nanos INT UNSIGNED, in operator_t CHAR(2))
BEGIN
	DECLARE data TEXT default CONCAT('(SELECT entity_id FROM transaction_log AS t WHERE t.transaction=\'', 
		transaction, 
		'\'',
		IF(userName IS NOT NULL, 
			CONCAT(' AND t.realm', operator_u, '? AND t.username', operator_u, '?'),
			'' 
		),
		IF(ilb IS NOT NULL, 
			CONCAT(" AND", constructDateTimeWhereClauseForColumn("t.seconds", "t.nanos", ilb, ilb_nanos, eub, eub_nanos, operator_t)),
			""
		),
		')'
	);

	SET @stmtTransactionStr = makeStmt(sourceSet,targetSet,data,NULL);
	PREPARE stmtTransactionFilter from @stmtTransactionStr;
	IF userName IS NOT NULL THEN
		SET @userName = userName;
		SET @realm = realm;
		EXECUTE stmtTransactionFilter USING @realm, @userName;
	ELSE
		EXECUTE stmtTransactionFilter;
	END IF;

END;
//
DELIMITER ;
