/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP FUNCTION IF EXISTS db_2_0.getDateTimeWhereClause;
DELIMITER //

CREATE FUNCTION db_2_0.getDateTimeWhereClause(vDateTime VARCHAR(255), operator CHAR(4)) RETURNS VARCHAR(20000) DETERMINISTIC
BEGIN
	DECLARE sep_loc INTEGER DEFAULT LOCATE('--',vDateTime);
    DECLARE vDateTimeLow VARCHAR(255) DEFAULT IF(sep_loc != 0, SUBSTRING_INDEX(vDateTime, '--',1), vDateTime);
    DECLARE vDateTimeUpp VARCHAR(255) DEFAULT IF(sep_loc != 0, SUBSTRING_INDEX(vDateTime, '--',-1), NULL);
	
    DECLARE vDateTimeSecLow VARCHAR(255) DEFAULT SUBSTRING_INDEX(vDateTimeLow, 'UTC', 1);
    DECLARE vDateTimeNSLow VARCHAR(255) DEFAULT IF(SUBSTRING_INDEX(vDateTimeLow, 'UTC', -1)='',NULL,SUBSTRING_INDEX(vDateTimeLow, 'UTC', -1));
	
    DECLARE vDateTimeSecUpp VARCHAR(255) DEFAULT IF(sep_loc != 0, SUBSTRING_INDEX(vDateTimeUpp, 'UTC', 1), NULL);
    DECLARE vDateTimeNSUpp VARCHAR(255) DEFAULT IF(sep_loc != 0 AND SUBSTRING_INDEX(vDateTimeUpp, 'UTC', -1)!='',SUBSTRING_INDEX(vDateTimeUpp, 'UTC', -1),NULL);
    
	
	RETURN constructDateTimeWhereClauseForColumn("subdata.value", "subdata.value_ns", vDateTimeSecLow, vDateTimeNSLow, vDateTimeSecUpp, vDateTimeNSUpp, operator);
END;
//
DELIMITER ;




DROP FUNCTION IF EXISTS db_2_0.constructDateTimeWhereClauseForColumn;
DELIMITER //

CREATE FUNCTION db_2_0.constructDateTimeWhereClauseForColumn(seconds_col VARCHAR(255), nanos_col VARCHAR(255), vDateTimeSecLow VARCHAR(255), vDateTimeNSLow VARCHAR(255), vDateTimeSecUpp VARCHAR(255), vDateTimeNSUpp VARCHAR(255), operator CHAR(4)) RETURNS VARCHAR(20000) DETERMINISTIC
BEGIN

	DECLARE isInterval BOOLEAN DEFAULT vDateTimeSecUpp IS NOT NULL or vDateTimeNSUpp IS NOT NULL; 
    DECLARE operator_prefix CHAR(1) DEFAULT LEFT(operator,1);

	IF isInterval THEN
		IF operator = '=' THEN
			RETURN " 0=1";
        ELSEIF operator = '!=' THEN
			RETURN " 0=1";
        ELSEIF operator = '>' or operator = '<=' THEN 
            RETURN CONCAT(" ", seconds_col, operator_prefix, vDateTimeSecUpp);
        ELSEIF operator = '<' or operator = '>=' THEN 
            RETURN CONCAT(" ", seconds_col, operator_prefix, vDateTimeSecLow);
		ELSEIF operator = "(" THEN
            RETURN CONCAT(" ", seconds_col, ">=", vDateTimeSecLow, " AND ",seconds_col, "<", vDateTimeSecUpp);
		ELSEIF operator = "!(" THEN
            RETURN CONCAT(" ", seconds_col, "<", vDateTimeSecLow, " OR ", seconds_col, ">=", vDateTimeSecUpp);
		END IF;
    ELSE
        IF operator = '=' THEN
            RETURN CONCAT(" ",
				seconds_col,
				"=", vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, CONCAT(' AND ', nanos_col, ' IS NULL'), CONCAT(' AND ',
					nanos_col,
				'=', vDateTimeNSLow)));
        ELSEIF operator = '!=' THEN
            RETURN CONCAT(" ",
				seconds_col,
				"!=", vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, '', CONCAT(' OR ',
						nanos_col,
						'!=', vDateTimeNSLow)));
        ELSEIF operator = '>' or operator = '<' THEN
            RETURN CONCAT(" ",
				seconds_col, operator, vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, '', CONCAT(' OR (',seconds_col,'=', vDateTimeSecLow, ' AND ',nanos_col, operator, vDateTimeNSLow, ')')));
        ELSEIF operator = '>=' or operator = '<=' THEN
            RETURN CONCAT(" ",seconds_col, operator, vDateTimeSecLow, IF(vDateTimeNSLow IS NULL, '', CONCAT(' AND (',seconds_col, operator_prefix, vDateTimeSecLow, ' OR ',nanos_col, operator, vDateTimeNSLow, ')')));
		ELSEIF operator = "(" THEN
            RETURN IF(vDateTimeNSLow IS NULL,CONCAT(" ",seconds_col,"=", vDateTimeSecLow),CONCAT(" ",seconds_col,"=",vDateTimeSecLow," AND ",nanos_col,"=",vDateTimeNSLow));
		ELSEIF operator = "!(" THEN
            RETURN IF(vDateTimeNSLow IS NULL,CONCAT(" ",seconds_col,"!=",vDateTimeSecLow, ""),CONCAT(" ",seconds_col,"!=",vDateTimeSecLow," OR ",nanos_col, " IS NULL OR ", nanos_col, "!=",vDateTimeNSLow));
        END IF;
    END IF;
    return ' 0=1';
END;
//
DELIMITER ;




DROP FUNCTION IF EXISTS db_2_0.getDateWhereClause;
DELIMITER //

CREATE FUNCTION db_2_0.getDateWhereClause(vDateTimeDotNotation VARCHAR(255), operator CHAR(4)) RETURNS VARCHAR(20000) DETERMINISTIC
BEGIN
	DECLARE isInterval INTEGER DEFAULT LOCATE('--',vDateTimeDotNotation);
	DECLARE vILB VARCHAR(255) DEFAULT IF(isInterval != 0, SUBSTRING_INDEX(vDateTimeDotNotation, '--', 1), vDateTimeDotNotation);
	DECLARE vEUB VARCHAR(255) DEFAULT IF(isInterval != 0, SUBSTRING_INDEX(vDateTimeDotNotation, '--', -1), NULL);
	DECLARE vILB_Date INTEGER DEFAULT SUBSTRING_INDEX(vILB, '.', 1);
	DECLARE vEUB_Date INTEGER DEFAULT SUBSTRING_INDEX(vEUB, '.', 1);
    DECLARE hasTime INTEGER DEFAULT LOCATE('.NULL.NULL',vILB);
    DECLARE dom INTEGER DEFAULT vILB_Date % 100;
    DECLARE mon INTEGER DEFAULT ((vILB_Date % 10000) - dom) / 100;    
    DECLARE yea INTEGER DEFAULT (vILB_Date - (vILB_Date%10000)) / 10000;
    SELECT vILB_Date != vEUB_Date INTO isInterval;

    IF operator = '=' and hasTime != 0 THEN
        RETURN CONCAT(" subdata.value=", vILB_Date);
    ELSEIF operator = "!=" and hasTime != 0 THEN
        IF mon != 0  and dom != 0 THEN
            RETURN CONCAT(" subdata.value!=", vILB_Date, " and subdata.value%100!=0"); 
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value!=", vILB_Date, " and subdata.value%100=0 and subdata.value%10000!=0");
        ELSE
            RETURN CONCAT(" subdata.value!=", vILB_Date, " and subdata.value%10000=0");
        END IF;
    ELSEIF operator = "(" and hasTime != 0 THEN
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value=", vILB_Date);
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value=",vILB_Date," OR (subdata.value>", vILB_Date, " and subdata.value<", vEUB_Date, " and subdata.value%10000!=0)");
        ELSE
            RETURN CONCAT(" subdata.value=",vILB_Date," OR (subdata.value>", vILB_Date, " and subdata.value<", vEUB_Date,")");
        END IF;
    ELSEIF operator = "!(" THEN
        IF hasTime = 0 THEN
            RETURN " 0=0";
        END IF;
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value!=",vILB_Date);
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" (subdata.value!=",vILB_Date, " AND subdata.value%100=0) OR ((subdata.value<", vILB_Date, " or subdata.value>", vEUB_Date, ") and subdata.value%100!=0)");
        ELSE
            RETURN CONCAT(" (subdata.value!=",vILB_Date, " AND subdata.value%10000=0) OR ((subdata.value<", vILB_Date, " or subdata.value>=", vEUB_Date, ") and subdata.value%10000!=0)");
        END IF;
    ELSEIF operator = "<" THEN
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value<", vILB_Date, " and (subdata.value%100!=0 or (subdata.value<", yea*10000+mon*100, " and subdata.value%10000!=0) or (subdata.value<", yea*10000, " and subdata.value%10000=0))");
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value<", vILB_Date, " and (subdata.value%10000!=0 or (subdata.value<", yea*10000, "))");
        ELSE
            RETURN CONCAT(" subdata.value<", vILB_Date);
        END IF;
    ELSEIF operator = ">" THEN
        IF mon != 0 and dom != 0 THEN
            RETURN CONCAT(" subdata.value>", vILB_Date);
        ELSEIF mon != 0 THEN
            RETURN CONCAT(" subdata.value>=",vEUB_Date);
        ELSE
            RETURN CONCAT(" subdata.value>=",vEUB_Date);
        END IF;
    END IF;

    return ' 0=1';
END;
//
DELIMITER ;
