/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP PROCEDURE IF EXISTS db_2_0.initEntity;
DELIMITER //

CREATE PROCEDURE db_2_0.initEntity(in eid INT UNSIGNED, in ename VARCHAR(255), in enameLike VARCHAR(255), in enameRegexp VARCHAR(255), in resultset VARCHAR(255))
initEntityLabel: BEGIN
	SET @initEntityStmtStr = NULL;

	IF ename IS NOT NULL THEN
		SET @initEntityStmtStr = CONCAT('INSERT IGNORE INTO `',resultset,'` (id) SELECT id FROM entities WHERE name=? and id>=100 UNION ALL SELECT entity_id FROM name_data WHERE value=?;');
		SET @query_param = ename;
	ELSEIF enameLike IS NOT NULL THEN
		SET @initEntityStmtStr = CONCAT('INSERT IGNORE INTO `',resultset,'` (id) SELECT id FROM entities WHERE name LIKE ? and id>=100 UNION ALL SELECT entity_id FROM name_data WHERE value LIKE ?;');
		SET @query_param = enameLike;
	ELSEIF enameRegexp IS NOT NULL THEN 
		SET @initEntityStmtStr = CONCAT('INSERT IGNORE INTO `',resultset,'` (id) SELECT id FROM entities WHERE name REGEXP ? and id>=100 UNION ALL SELECT entity_id FROM name_data WHERE value REGEXP ?;');
		SET @query_param = enameRegexp;
    END IF;

	IF @initEntityStmtStr IS NOT NULL THEN
		PREPARE initEntityStmt FROM @initEntityStmtStr;
		EXECUTE initEntityStmt USING @query_param, @query_param;
		DEALLOCATE PREPARE initEntityStmt;
    END IF;
	
    IF eid IS NOT NULL THEN
		SET @initEntityStmtStr = CONCAT('INSERT IGNORE INTO `',resultset,'` (id) SELECT id FROM entities WHERE id=',eid,';');
		PREPARE initEntityStmt FROM @initEntityStmtStr;
		EXECUTE initEntityStmt;
		DEALLOCATE PREPARE initEntityStmt;
    END IF;

	
	IF @initEntityStmtStr IS NOT NULL THEN
    	call getChildren(resultset);
	END IF;

END;
//
DELIMITER ;




DROP PROCEDURE IF EXISTS db_2_0.applyRole;
