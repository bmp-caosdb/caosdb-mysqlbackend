/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP PROCEDURE IF EXISTS db_2_0.initSubProperty;
DELIMITER //

CREATE PROCEDURE db_2_0.initSubProperty(in sourceSet VARCHAR(255), in propertiesTable VARCHAR(255), in refIdsTable VARCHAR(255))
BEGIN
DECLARE newTableName VARCHAR(255) DEFAULT NULL;
    call registerTempTableName(newTableName);	
    
    SET @createSubPropertyListTableStr = CONCAT('CREATE TEMPORARY TABLE `', newTableName,'` ( entity_id INT UNSIGNED NOT NULL, id INT UNSIGNED NOT NULL, domain INT UNSIGNED NOT NULL, CONSTRAINT `',newTableName,'PK` PRIMARY KEY (entity_id, id, domain)) ' );
    
    PREPARE createSubPropertyListTable FROM @createSubPropertyListTableStr; 
    EXECUTE createSubPropertyListTable;
    DEALLOCATE PREPARE createSubPropertyListTable;

	SET @subResultSetStmtStr = CONCAT('INSERT IGNORE INTO `', newTableName, '` (domain, entity_id, id) 
            SELECT data1.domain_id as domain, data1.entity_id as entity_id, data1.value as id 
                FROM reference_data as data1 JOIN reference_data as data2 
                    ON (data1.domain_id=0 
                        AND data1.domain_id=data2.domain_id 
                        AND data2.entity_id=data1.entity_id 
                        AND (
                            (data1.property_id=data2.value AND data2.status="REPLACEMENT")
                            OR
                            (data1.property_id!=data2.value AND data2.status!="REPLACEMENT" AND data1.status!="REPLACEMENT" AND data1.property_id=data2.property_id)
                        )
                        AND EXISTS (SELECT 1 FROM `', sourceSet, '` as source WHERE source.id=data1.entity_id LIMIT 1)',
                        IF(propertiesTable IS NULL, '', CONCAT(' AND EXISTS (SELECT 1 FROM `', propertiesTable, '` as props WHERE props.id=data2.property_id LIMIT 1)')),
                        IF(refIdsTable IS NULL, '', CONCAT(' AND EXISTS (SELECT 1 FROM `', refIdsTable, '` as refs WHERE refs.id=data1.value LIMIT 1)')),	
		')'
        );


	PREPARE subResultSetStmt FROM @subResultSetStmtStr;
	EXECUTE subResultSetStmt;
    DEALLOCATE PREPARE subResultSetStmt;

	SELECT newTableName as list;

END;
//
DELIMITER ;
