/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */



delimiter //

drop procedure if exists db_2_0.retrieveEntity //

create procedure db_2_0.retrieveEntity(in EntityID INT UNSIGNED)
BEGIN
	DECLARE FilePath VARCHAR(255) DEFAULT NULL;
	DECLARE FileSize VARCHAR(255) DEFAULT NULL;
	DECLARE FileHash VARCHAR(255) DEFAULT NULL;
	DECLARE DatatypeID INT UNSIGNED DEFAULT NULL;
    DECLARE CollectionName VARCHAR(255) DEFAULT NULL;
	
	Select path, size, hex(hash) into FilePath, FileSize, FileHash from files where file_id = EntityID LIMIT 1;
	Select datatype into DatatypeID from data_type where domain_id=0 and entity_id=0 and property_id=EntityID LIMIT 1;

	SELECT collection into CollectionName from collection_type where domain_id=0 and entity_id=0 and property_id=EntityID LIMIT 1;

	Select 
		(Select name from entities where id=DatatypeID) as Datatype, 
		CollectionName as Collection,
		EntityID as EntityID, 
		e.name as EntityName, 
		e.description as EntityDesc, 
		e.role as EntityRole, 
		FileSize as FileSize, 
		FilePath as FilePath, 
		FileHash as FileHash,
		(SELECT acl FROM entity_acl as a WHERE a.id = e.acl) as ACL
	from entities e where id = EntityID LIMIT 1;
END;
//


delimiter ;
