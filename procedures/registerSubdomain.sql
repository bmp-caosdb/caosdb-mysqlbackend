/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */








DROP PROCEDURE IF EXISTS db_2_0.registerSubdomain;
delimiter //

CREATE PROCEDURE db_2_0.registerSubdomain(in Count INT UNSIGNED)
BEGIN
    DECLARE ED INTEGER DEFAULT NULL;

Select COUNT(id) INTO ED FROM entities WHERE Role='DOMAIN' AND id!=0;
WHILE ED < Count DO
		INSERT INTO entities (name, description, role, acl) VALUES (NULL, "Multipurpose subdomain", 'DOMAIN', 0);
		SET ED = ED + 1;
END WHILE;

SELECT id as DomainID FROM entities WHERE Role='DOMAIN' and id!=0;

END;
//
delimiter ;
