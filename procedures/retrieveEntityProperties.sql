/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */


delimiter //

drop procedure if exists db_2_0.retrieveEntityProperties //

create procedure db_2_0.retrieveEntityProperties(in DomainID INT UNSIGNED, in EntityID INT UNSIGNED)
BEGIN

		#-- double properties
		Select 
		property_id as PropertyID, value as PropertyValue, status as PropertyStatus, pidx as PropertyIndex from double_data where domain_id = DomainID and entity_id = EntityID

		UNION ALL
		
		#-- integer properties
		Select 
		property_id as PropertyID, value as PropertyValue, status as PropertyStatus, pidx as PropertyIndex from integer_data where domain_id = DomainID and entity_id = EntityID

		UNION ALL

		#-- date properties
		Select 
		property_id as PropertyID, CONCAT(value, '.NULL.NULL') as PropertyValue, status as PropertyStatus, pidx as PropertyIndex from date_data where domain_id = DomainID and entity_id = EntityID

		UNION ALL

		#-- datetime properties
		Select 
		property_id as PropertyID, CONCAT(value, 'UTC', IF(value_ns IS NULL, '', value_ns)) as PropertyValue, status as PropertyStatus, pidx as PropertyIndex from datetime_data where domain_id = DomainID and entity_id = EntityID

		UNION ALL

		#-- text properties
		Select 
		property_id as PropertyID, value as PropertyValue, status as PropertyStatus, pidx as PropertyIndex from text_data where domain_id = DomainID and entity_id = EntityID

		UNION ALL

		#-- enum properties
		Select 
		property_id as PropertyID, value as PropertyValue, status as PropertyStatus, pidx as PropertyIndex from enum_data where domain_id = DomainID and entity_id = EntityID

		UNION ALL

		#-- reference properties
		Select 
		property_id as PropertyID, value as PropertyValue, status as PropertyStatus, pidx as PropertyIndex from reference_data where domain_id = DomainID and entity_id = EntityID

		UNION ALL

		#-- null properties
		Select
		property_id as PropertyID, NULL AS PropertyValue, status as PropertyStatus, pidx as PropertyIndex from null_data WHERE domain_id = DomainID and entity_id = EntityID
		
		UNION ALL

		#-- name properties
		Select 
		property_id as PropertyID, value as PropertyValue, status as PropertyStatus, pidx as PropertyIndex from name_data where domain_id = DomainID and entity_id = EntityID;



END;
//


delimiter ;
