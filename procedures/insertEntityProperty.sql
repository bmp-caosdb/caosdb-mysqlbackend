/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

DROP PROCEDURE IF EXISTS db_2_0.insertEntityProperty;
delimiter //
CREATE PROCEDURE db_2_0.insertEntityProperty(in DomainID INT UNSIGNED, in EntityID INT UNSIGNED, in PropertyID INT UNSIGNED, in Datatable VARCHAR(255), in PropertyValue TEXT, in PropertyUnitSig BIGINT, in PropertyStatus VARCHAR(255), in NameOverride VARCHAR(255), in DescOverride TEXT, in datatypeOverride INT UNSIGNED, in Collection VARCHAR(255), in PropertyIndex INT UNSIGNED)
BEGIN

	CASE Datatable
	WHEN 'double_data' THEN
		INSERT INTO double_data 
		(domain_id, entity_id, property_id, value, unit_sig, status, pidx) 
		VALUES 
		(DomainID, EntityID, PropertyID, PropertyValue, PropertyUnitSig, PropertyStatus, PropertyIndex);
	WHEN 'integer_data' THEN
		INSERT INTO integer_data 
		(domain_id, entity_id, property_id, value, unit_sig, status, pidx) 
		VALUES 
		(DomainID, EntityID, PropertyID, PropertyValue, PropertyUnitSig, PropertyStatus, PropertyIndex);
	WHEN 'datetime_data' THEN
		INSERT INTO datetime_data 
		(domain_id, entity_id, property_id, value, value_ns, status, pidx) 
		VALUES 
		(DomainID, EntityID, PropertyID, SUBSTRING_INDEX(PropertyValue, 'UTC', 1), IF(SUBSTRING_INDEX(PropertyValue, 'UTC', -1)='',NULL,SUBSTRING_INDEX(PropertyValue, 'UTC', -1)), PropertyStatus, PropertyIndex);
	WHEN 'reference_data' THEN
		INSERT INTO reference_data 
		(domain_id, entity_id, property_id, value, status, pidx) 
		VALUES 
		(DomainID, EntityID, PropertyID, PropertyValue, PropertyStatus, PropertyIndex);
	WHEN 'enum_data' THEN	
		INSERT INTO enum_data 
		(domain_id, entity_id, property_id, value, status, pidx) 
		VALUES 
		(DomainID, EntityID, PropertyID, PropertyValue, PropertyStatus, PropertyIndex);
	WHEN 'date_data' THEN	
		INSERT INTO date_data 
		(domain_id, entity_id, property_id, value, status, pidx) 
		VALUES 
		(DomainID, EntityID, PropertyID, SUBSTRING_INDEX(PropertyValue, '.', 1), PropertyStatus, PropertyIndex);
	WHEN 'text_data' THEN
		INSERT INTO text_data 
		(domain_id, entity_id, property_id, value, status, pidx) 
		VALUES 
		(DomainID, EntityID, PropertyID, PropertyValue, PropertyStatus, PropertyIndex);
	WHEN 'null_data' THEN
		INSERT INTO null_data
		(domain_id, entity_id, property_id, status, pidx) 
		VALUES 
		(DomainID, EntityID, PropertyID, PropertyStatus, PropertyIndex);
	WHEN 'name_data' THEN
		INSERT INTO name_data
		(domain_id, entity_id, property_id, value, status, pidx) 
		VALUES 
		(DomainID, EntityID, PropertyID, PropertyValue, PropertyStatus, PropertyIndex);

	ELSE	
		SELECT * FROM table_does_not_exist;
	END CASE;

	IF DatatypeOverride IS NOT NULL THEN
		call overrideType(DomainID, EntityID, PropertyID, DatatypeOverride);
		IF Collection IS NOT NULL THEN
			INSERT INTO collection_type (domain_id, entity_id, property_id, collection) VALUES (DomainID, EntityID, PropertyID, Collection);
		END IF;
	END IF;

	IF NameOverride IS NOT NULL THEN
		call overrideName(DomainID, EntityID, PropertyID, NameOverride);	
	END IF;

	IF DescOverride IS NOT NULL THEN
		call overrideDesc(DomainID, EntityID, PropertyID, DescOverride);
	END IF;

END;
//
delimiter ;
