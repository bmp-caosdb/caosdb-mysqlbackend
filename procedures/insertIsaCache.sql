/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
DROP PROCEDURE IF EXISTS db_2_0.insertIsa;
DELIMITER //

CREATE PROCEDURE db_2_0.insertIsa(IN c INT UNSIGNED, IN p INT UNSIGNED)
BEGIN

    INSERT INTO isa_cache (child, parent, rpath) VALUES (c,p,c);
	
    -- foreach supertype of p
    --     INSERT (c, supertype, p);
    INSERT IGNORE INTO isa_cache SELECT c AS child, i.parent AS parent, IF(p=i.rpath or i.rpath=parent, p, concat(p, ">", i.rpath)) AS rpath FROM isa_cache AS i WHERE i.child = p;
    
    
    -- foreach subtype of c insert each supertype of p
    INSERT IGNORE INTO isa_cache SELECT l.child, r.parent, if(l.rpath=l.child and r.rpath=c, c, concat(if(l.rpath=l.child,c,concat(l.rpath, '>', c)), if(r.rpath=c,'',concat('>', r.rpath)))) AS rpath FROM isa_cache as l INNER JOIN isa_cache as r ON (l.parent = r.child AND l.parent=c);


	
END;
//

DELIMITER ;
