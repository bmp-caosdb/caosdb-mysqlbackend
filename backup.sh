#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

#dump a database with all procedures, permissions, structure and data

BACKUPDIR=./backup #The directory which the dump is to be stored to. Do not change it here. Use the --backupdir=./my/dir/ option!
PRINT_HELP="--backupdir=BACKUPDIR\n\tThe directory which the dump is to be stored to. (Defaults to ./backup)\n"

. .config
# load useful stuff - scans the parameters and so on...
. patches/utils/patch_header.sh

#create backup dir if not exists
[ -d $BACKUPDIR ] || mkdir $BACKUPDIR



function backup {
    NARG_NAME=$1
    NARG_FILE=$2
    shift 2
    echo $MYSQLDUMP_CMD
	# parameters: connection, database, outfile
	if [ -e "$NARG_FILE" ]; then
		failure "dumpfile already exists."
	fi
    echo "Dumping database $NARG_NAME to $NARG_FILE ... "
	$CMD_MYSQL_DUMP $MYSQL_CONNECTION_NO_DB $* --opt --default-character-set=utf8 --routines $NARG_NAME > $NARG_FILE
	success
}





##test dump file exists
#touch dumpfile.tmp
#backup 0 1 2 "dumpfile.tmp"
#rm dumpfile.tmp


DATE=$(date -u --rfc-3339=ns | sed 's/ /T/g')
BACKUPFILE=${BACKUPDIR}/${DATABASE_NAME}.${DATE}.dump.sql
backup $DATABASE_NAME $BACKUPFILE


