# Setup of the CaosDB SQL back end

## Dependencies
* `MariaDB Client 10.1`, `MySQL Client 5.5`, or later versions.  In the case of
  MySQL, version 5.6 is recommended.

## Create the configuration
* Run `./configure`. For the default values and the meaning of these default
  values see [config.defaults](config.defaults). The `configure` script also
  prompts for a (new) user password. Remember that one, since it will be the
  password of the mysql user which is used by the caosdb server.  Upon succesful
  completion, the `configure` script writes the configuration to `.config`.
    * If there is no `mysql-config-editor` (`MySQL 5.5`. and `MariaDB`) then the
      user is prompted for the `MYSQL_USER_PASSWORD`, that is the password of
      the `MYSQL_USER`. The plain password is stored in the `.config` file. So
      make sure nobody else can read it.
    * If you are using MariaDB and the `root` database user uses pam authentication,
      no pasword string is required. When prompted for the `MYSQL_USER_PASSWORD`
      just leave it blank and hit enter. But you need be login as root for the
      installation and upgrade process.

## Setup the SQL database for CaosDB

* Run `make install`. If a there is a database with the name you have choosen
  during the configuration, you need to reconfigure or delete the database
  first.

## Upgrade the SQL database

* Run `make upgrade`. This upgrades the database to the last version.

## Drop Database

* If you want to delete your database, run `make drop-$DATABASE_NAME`. **Use
  this with caution!!!** If you did not backup your database, **everything will
  be lost afterwards.** And no, there is *no* additional prompt to ask if you
  are sure.  If you `make drop-...`, you *actually* delete the database.
