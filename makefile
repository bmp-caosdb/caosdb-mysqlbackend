#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
SHELL=/bin/bash

include .config

INSTALL_SQL_FILE=db_2_0.sql
ifdef LOGIN_PATH
	M_MYSQL_CONNECTION=--login-path=$(M_LOGIN_PATH)
else
	MYSQL_CONNECTION=--host="$(MYSQL_HOST)" --port="$(MYSQL_PORT)"	\
		--user="$(MYSQL_USER)" --password="$(MYSQL_USER_PASSWORD)"
endif

.PHONY: test-connection
test-connection:
	./make_db test-connection

.PHONY: upgrade
upgrade:
	@cd patches; ./applyPatches.sh --env=../.config

.PHONY: install
install: _install _grant upgrade

.PHONY: _install
_install:
	./make_db install_db

.PHONY: _grant
_grant:
	./make_db grant

# Drop the user and a given database
.PHONY: drop-%
drop-%:
	./make_db drop $(patsubst drop-%,%,$@)
