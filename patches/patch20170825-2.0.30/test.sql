/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
SET @@SESSION.max_sp_recursion_depth=25;

SET FOREIGN_KEY_CHECKS = 0;
delete from isa_cache;
-- simple 1->0

call insertIsa(1,0);
select "subtypes:";
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(0,1);

-- add 2->1

call insertIsa(2,1);
select "subtypes:";
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(1,2);
call isSubtype(0,2);
call isSubtype(0,1);

-- add 3->2
call insertIsa(3,2);
select "subtypes:";
call isSubtype(3,2);
call isSubtype(3,1);
call isSubtype(3,0);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(2,3);
call isSubtype(1,3);
call isSubtype(0,3);
call isSubtype(1,2);
call isSubtype(0,2);
call isSubtype(0,1);

-- add 4->2
call insertIsa(4,2);
select "subtypes:";
call isSubtype(4,2);
call isSubtype(4,1);
call isSubtype(4,0);
call isSubtype(3,2);
call isSubtype(3,2);
call isSubtype(3,1);
call isSubtype(3,0);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(3,4);
call isSubtype(2,4);
call isSubtype(1,4);
call isSubtype(0,4);
call isSubtype(2,3);
call isSubtype(1,3);
call isSubtype(0,3);
call isSubtype(1,2);
call isSubtype(0,2);
call isSubtype(0,1);

-- add 5->3
call insertIsa(5,3);
select "subtypes:";
call isSubtype(5,3);
call isSubtype(5,2);
call isSubtype(5,1);
call isSubtype(5,0);
call isSubtype(4,2);
call isSubtype(4,1);
call isSubtype(4,0);
call isSubtype(3,2);
call isSubtype(3,2);
call isSubtype(3,1);
call isSubtype(3,0);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(4,5);
call isSubtype(3,5);
call isSubtype(2,5);
call isSubtype(1,5);
call isSubtype(0,5);
call isSubtype(5,4);
call isSubtype(3,4);
call isSubtype(2,4);
call isSubtype(1,4);
call isSubtype(0,4);
call isSubtype(2,3);
call isSubtype(1,3);
call isSubtype(0,3);
call isSubtype(1,2);
call isSubtype(0,2);
call isSubtype(0,1);

-- add 5->4
call insertIsa(5,4);
select "subtypes:";
call isSubtype(5,4);
call isSubtype(5,3);
call isSubtype(5,2);
call isSubtype(5,1);
call isSubtype(5,0);
call isSubtype(4,2);
call isSubtype(4,1);
call isSubtype(4,0);
call isSubtype(3,2);
call isSubtype(3,2);
call isSubtype(3,1);
call isSubtype(3,0);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(4,5);
call isSubtype(3,5);
call isSubtype(2,5);
call isSubtype(1,5);
call isSubtype(0,5);
call isSubtype(3,4);
call isSubtype(2,4);
call isSubtype(1,4);
call isSubtype(0,4);
call isSubtype(2,3);
call isSubtype(1,3);
call isSubtype(0,3);
call isSubtype(1,2);
call isSubtype(0,2);
call isSubtype(0,1);

-- add 6->5
call insertIsa(6,5);
select "subtypes:";
call isSubtype(6,5);
call isSubtype(6,4);
call isSubtype(6,3);
call isSubtype(6,2);
call isSubtype(6,1);
call isSubtype(6,0);
call isSubtype(5,4);
call isSubtype(5,3);
call isSubtype(5,2);
call isSubtype(5,1);
call isSubtype(5,0);
call isSubtype(4,2);
call isSubtype(4,1);
call isSubtype(4,0);
call isSubtype(3,2);
call isSubtype(3,2);
call isSubtype(3,1);
call isSubtype(3,0);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(5,6);
call isSubtype(4,6);
call isSubtype(3,6);
call isSubtype(2,6);
call isSubtype(1,6);
call isSubtype(0,6);
call isSubtype(4,5);
call isSubtype(3,5);
call isSubtype(2,5);
call isSubtype(1,5);
call isSubtype(0,5);
call isSubtype(3,4);
call isSubtype(2,4);
call isSubtype(1,4);
call isSubtype(0,4);
call isSubtype(2,3);
call isSubtype(1,3);
call isSubtype(0,3);
call isSubtype(1,2);
call isSubtype(0,2);
call isSubtype(0,1);

-- remove 2->1 
CREATE TEMPORARY TABLE expected1 SELECT * from isa_cache;
call deleteIsa(2);
select "subtypes:";
call isSubtype(6,5);
call isSubtype(6,4);
call isSubtype(6,3);
call isSubtype(6,2);
call isSubtype(5,4);
call isSubtype(5,3);
call isSubtype(5,2);
call isSubtype(4,2);
call isSubtype(3,2);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(3,1);
call isSubtype(3,0);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(6,0);
call isSubtype(6,1);
call isSubtype(5,6);
call isSubtype(5,1);
call isSubtype(5,0);
call isSubtype(4,6);
call isSubtype(4,1);
call isSubtype(4,0);
call isSubtype(3,6);
call isSubtype(2,6);
call isSubtype(1,6);
call isSubtype(0,6);
call isSubtype(4,5);
call isSubtype(3,5);
call isSubtype(2,5);
call isSubtype(1,5);
call isSubtype(0,5);
call isSubtype(3,4);
call isSubtype(2,4);
call isSubtype(1,4);
call isSubtype(0,4);
call isSubtype(2,3);
call isSubtype(1,3);
call isSubtype(0,3);
call isSubtype(1,2);
call isSubtype(0,2);
call isSubtype(0,1);


-- and add 2->1 again
call insertIsa(2,1);
SELECT A.*, B.* FROM isa_cache AS A LEFT JOIN expected1 AS B ON (A.child = B.child AND A.parent=B.parent AND A.rpath=B.rpath) WHERE A.child IS NULL OR B.child IS NULL; 
SELECT A.*, B.* FROM isa_cache AS A RIGHT JOIN expected1 AS B ON (A.child = B.child AND A.parent=B.parent AND A.rpath=B.rpath) WHERE A.child IS NULL OR B.child IS NULL;

-- remove 4->2
call deleteIsa(4);

select "subtypes:";
call isSubtype(6,0);
call isSubtype(6,1);
call isSubtype(6,2);
call isSubtype(6,3);
call isSubtype(6,4);
call isSubtype(6,5);
call isSubtype(5,0);
call isSubtype(5,1);
call isSubtype(5,2);
call isSubtype(5,3);
call isSubtype(5,4);
call isSubtype(3,2);
call isSubtype(3,1);
call isSubtype(3,0);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(0,1);
call isSubtype(0,2);
call isSubtype(0,3);
call isSubtype(0,4);
call isSubtype(0,5);
call isSubtype(0,6);
call isSubtype(1,2);
call isSubtype(1,3);
call isSubtype(1,4);
call isSubtype(1,5);
call isSubtype(1,6);
call isSubtype(2,3);
call isSubtype(2,4);
call isSubtype(2,5);
call isSubtype(2,6);
call isSubtype(3,4);
call isSubtype(3,5);
call isSubtype(3,6);
call isSubtype(4,0);
call isSubtype(4,1);
call isSubtype(4,2);
call isSubtype(4,3);
call isSubtype(4,5);
call isSubtype(4,6);
call isSubtype(5,6);

-- and add 4->2 again
call insertIsa(4,2);
SELECT A.*, B.* FROM isa_cache AS A LEFT JOIN expected1 AS B ON (A.child = B.child AND A.parent=B.parent AND A.rpath=B.rpath) WHERE A.child IS NULL OR B.child IS NULL; 
SELECT A.*, B.* FROM isa_cache AS A RIGHT JOIN expected1 AS B ON (A.child = B.child AND A.parent=B.parent AND A.rpath=B.rpath) WHERE A.child IS NULL OR B.child IS NULL;

-- remove 5->4 and 5->3
call deleteIsa(5);
select "subtypes:";
call isSubtype(6,5);
call isSubtype(4,2);
call isSubtype(4,1);
call isSubtype(4,0);
call isSubtype(3,2);
call isSubtype(3,1);
call isSubtype(3,0);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(6,0);
call isSubtype(6,1);
call isSubtype(6,2);
call isSubtype(6,3);
call isSubtype(6,4);
call isSubtype(5,6);
call isSubtype(5,4);
call isSubtype(5,3);
call isSubtype(5,2);
call isSubtype(5,1);
call isSubtype(5,0);
call isSubtype(4,3);
call isSubtype(4,5);
call isSubtype(4,6);
call isSubtype(3,4);
call isSubtype(3,5);
call isSubtype(3,6);
call isSubtype(2,3);
call isSubtype(2,4);
call isSubtype(2,5);
call isSubtype(2,6);
call isSubtype(1,2);
call isSubtype(1,3);
call isSubtype(1,4);
call isSubtype(1,5);
call isSubtype(1,6);
call isSubtype(0,1);
call isSubtype(0,2);
call isSubtype(0,3);
call isSubtype(0,4);
call isSubtype(0,5);
call isSubtype(0,6);

-- and add 5->4 again
call insertIsa(5,4);

select "subtypes:";
call isSubtype(6,5);
call isSubtype(6,4);
call isSubtype(6,2);
call isSubtype(6,1);
call isSubtype(6,0);
call isSubtype(5,4);
call isSubtype(5,2);
call isSubtype(5,1);
call isSubtype(5,0);
call isSubtype(4,2);
call isSubtype(4,1);
call isSubtype(4,0);
call isSubtype(3,2);
call isSubtype(3,1);
call isSubtype(3,0);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(1,0);
select "NOT subtypes:";
call isSubtype(0,1);
call isSubtype(0,2);
call isSubtype(0,3);
call isSubtype(0,4);
call isSubtype(0,5);
call isSubtype(0,6);
call isSubtype(1,2);
call isSubtype(1,3);
call isSubtype(1,4);
call isSubtype(1,5);
call isSubtype(1,6);
call isSubtype(2,3);
call isSubtype(2,4);
call isSubtype(2,5);
call isSubtype(2,6);
call isSubtype(3,4);
call isSubtype(3,5);
call isSubtype(3,6);
call isSubtype(4,3);
call isSubtype(4,5);
call isSubtype(4,6);
call isSubtype(5,3);
call isSubtype(5,6);
call isSubtype(6,3);


-- and add 5->3 again
call insertIsa(5,3);
SELECT A.*, B.* FROM isa_cache AS A LEFT JOIN expected1 AS B ON (A.child = B.child AND A.parent=B.parent AND A.rpath=B.rpath) WHERE A.child IS NULL OR B.child IS NULL; 
SELECT A.*, B.* FROM isa_cache AS A RIGHT JOIN expected1 AS B ON (A.child = B.child AND A.parent=B.parent AND A.rpath=B.rpath) WHERE A.child IS NULL OR B.child IS NULL;

-- add cycle 0->3
call insertIsa(0,3);
select "subtypes:";
call isSubtype(0,1);
call isSubtype(0,2);
call isSubtype(0,3);
call isSubtype(1,2);
call isSubtype(1,3);
call isSubtype(1,0);
call isSubtype(2,3);
call isSubtype(2,1);
call isSubtype(2,0);
call isSubtype(3,0);
call isSubtype(3,1);
call isSubtype(3,2);

-- remove cycle 0->3
call deleteIsa(0);
SELECT A.*, B.* FROM isa_cache AS A LEFT JOIN expected1 AS B ON (A.child = B.child AND A.parent=B.parent AND A.rpath=B.rpath) WHERE A.child IS NULL OR B.child IS NULL; 
SELECT A.*, B.* FROM isa_cache AS A RIGHT JOIN expected1 AS B ON (A.child = B.child AND A.parent=B.parent AND A.rpath=B.rpath) WHERE A.child IS NULL OR B.child IS NULL;

call deleteIsa(1);
call deleteIsa(2);
call deleteIsa(3);
call deleteIsa(4);
call deleteIsa(5);
call deleteIsa(6);

SET FOREIGN_KEY_CHECKS = 1;
