#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# this patch adds unit_sig columns to double_data and integer_data and creates the table units_lin_con
# Update mysql schema from version v2.0.4 to version v2.0.5
NEW_VERSION="v2.0.8"
OLD_VERSION="v2.0.7"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*

check_version $OLD_VERSION

function add_unit_sig_columns {
    mysql_execute 'ALTER TABLE double_data ADD COLUMN unit_sig BIGINT NULL DEFAULT NULL;'
    mysql_execute 'ALTER TABLE integer_data ADD COLUMN unit_sig BIGINT NULL DEFAULT NULL;'
}

function create_units_lin_con {
	mysql_execute 'CREATE TABLE units_lin_con (
	    signature_from BIGINT NOT NULL PRIMARY KEY,
		signature_to BIGINT NOT NULL,
		a DECIMAL(65,30) NOT NULL,
	    b_dividend INT NOT NULL,
	    b_divisor INT NOT NULL,
		c DECIMAL(65,30) NOT NULL
	) ENGINE=InnoDB;'
}

add_unit_sig_columns
create_units_lin_con


update_version $NEW_VERSION
success
