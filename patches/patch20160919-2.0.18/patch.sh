#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# longer PropertyIndeces 
# Update mysql schema to version v2.0.17
NEW_VERSION="v2.0.18"
OLD_VERSION="v2.0.17"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*


function mod_table {
	mysql_execute "ALTER TABLE \`$1\` MODIFY COLUMN \`pidx\` INT UNSIGNED NOT NULL DEFAULT 0;"
}

check_version $OLD_VERSION

mod_table "text_data"
mod_table "double_data"
mod_table "integer_data"
mod_table "enum_data"
mod_table "date_data"
mod_table "datetime_data"
mod_table "reference_data"

update_version $NEW_VERSION

success
