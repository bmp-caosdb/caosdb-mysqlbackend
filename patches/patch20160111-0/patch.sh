#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# adds a property_id column to name_overrides and desc_overrides.
# Update mysql schema to version v2.0.5
NEW_VERSION="v2.0.5"
OLD_VERSION="v2.0.4"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*

check_version $OLD_VERSION

function create_new_table {
	mysql_execute 'CREATE TABLE name_overrides2 (
		domain_id INT UNSIGNED,
		entity_id INT UNSIGNED,
		property_id INT UNSIGNED,
		name VARCHAR(255),
		INDEX `name_ov_dom_ent_idx` (domain_id,entity_id),
		FOREIGN KEY `name_ov_forkey_dom` (`domain_id`) REFERENCES `entities` (`id`),
		FOREIGN KEY `name_ov_forkey_ent` (`entity_id`) REFERENCES `entities` (`id`),
		FOREIGN KEY `name_ov_forkey_pro` (`property_id`) REFERENCES `entities` (`id`)
	) ENGINE=InnoDB;
	'
	mysql_execute 'CREATE TABLE desc_overrides2 (
		domain_id INT UNSIGNED,
		entity_id INT UNSIGNED,
		property_id INT UNSIGNED,
		description TEXT,
		INDEX `desc_ov_dom_ent_idx` (domain_id,entity_id),
		FOREIGN KEY `desc_ov_forkey_dom` (`domain_id`) REFERENCES `entities` (`id`),
		FOREIGN KEY `desc_ov_forkey_ent` (`entity_id`) REFERENCES `entities` (`id`),
		FOREIGN KEY `desc_ov_forkey_pro` (`property_id`) REFERENCES `entities` (`id`)
	) ENGINE=InnoDB;
	'
}

function copy_old {
	mysql_execute 'INSERT INTO name_overrides2 (domain_id, entity_id, property_id, name) SELECT 0, domain_id, entity_id, name FROM name_overrides;'
	mysql_execute 'INSERT INTO desc_overrides2 (domain_id, entity_id, property_id, description) SELECT 0, domain_id, entity_id, description FROM desc_overrides;'
	mysql_execute 'UPDATE name_overrides2,reference_data SET name_overrides2.domain_id=reference_data.domain_id WHERE name_overrides2.entity_id=reference_data.entity_id and name_overrides2.property_id=reference_data.value and status="REPLACEMENT" and reference_data.domain_id!=0;'
	mysql_execute 'UPDATE desc_overrides2,reference_data SET desc_overrides2.domain_id=reference_data.domain_id WHERE desc_overrides2.entity_id=reference_data.entity_id and desc_overrides2.property_id=reference_data.value and status="REPLACEMENT" and reference_data.domain_id!=0;'
}

function delete_rename {
	mysql_execute 'DROP TABLE name_overrides;'
	mysql_execute 'DROP TABLE desc_overrides;'
	mysql_execute 'ALTER TABLE name_overrides2 RENAME name_overrides;'
	mysql_execute 'ALTER TABLE desc_overrides2 RENAME desc_overrides;'
}

function set_idx {
	mysql_execute 'ALTER TABLE name_overrides ADD UNIQUE KEY `name_ov_ukey` (domain_id,entity_id,property_id);'
	mysql_execute 'ALTER TABLE desc_overrides ADD UNIQUE KEY `desc_ov_ukey` (domain_id,entity_id,property_id);'
}

create_new_table
copy_old
delete_rename
set_idx
update_version $NEW_VERSION

success
