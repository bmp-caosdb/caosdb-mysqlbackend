#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# this patch introduces the pidx colums for the five *_data tables
# Update mysql schema from version v2.0.0 to version v2.0.1
NEW_VERSION="v2.0.1"
OLD_VERSION="v2.0.0"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*

check_version $OLD_VERSION

function addIdx {
    $CMD_MYSQL $MYSQL_CONNECTION -D $DATABASE -e "ALTER TABLE $1 ADD COLUMN pidx TINYINT UNSIGNED NOT NULL DEFAULT 0;"
}

addIdx "reference_data"
addIdx "double_data"
addIdx "integer_data"
addIdx "datetime_data"
addIdx "text_data"

update_version $NEW_VERSION
success
