#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# Add entity_acl table
# Update mysql schema to version v2.0.22
NEW_VERSION="v2.0.22"
OLD_VERSION="v2.0.21"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*


check_version $OLD_VERSION

mysql_execute 'CREATE TABLE IF NOT EXISTS entity_acl (id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, acl VARBINARY(65525) NOT NULL) ENGINE=InnoDB; INSERT INTO entity_acl (acl) VALUES (""); UPDATE  entity_acl SET id = 0; ALTER TABLE entities ADD CONSTRAINT entity_entity_acl FOREIGN KEY (acl) REFERENCES entity_acl (id); CREATE INDEX entity_acl_acl ON entity_acl (acl(3072));'


update_version $NEW_VERSION

success
