#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# this patch introduces the mysql stored funtion 'CaosDBVersion' with a initial value of 2.0.0
# Update mysql schema to version v2.0.0

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh 
if test ! $(mysql_execute "Select CaosDBVersion();" > /dev/null) ; then
    uptodate
fi

# install function CaosDBVersion()
mysql_execute "CREATE FUNCTION CaosDBVersion() RETURNS VARCHAR(255) DETERMINISTIC RETURN 'v2.0.0';"

# create transaction_log table with new schema
mysql_execute "CREATE TABLE new_transaction_log (
 transaction VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'TRANSACTION.',
 user_id INT UNSIGNED NOT NULL COMMENT 'User.',
 date INT NOT NULL COMMENT 'Date of transaction.',
 time MEDIUMINT UNSIGNED NOT NULL COMMENT 'Time of transaction.',
 ns INT UNSIGNED NULL COMMENT 'Nanosecond part of transaction time.',
 entity_id INT UNSIGNED NOT NULL COMMENT 'Entity ID.',
 INDEX (entity_id), INDEX (date, time)
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_bin;"

# copy from old transaction_log
mysql_execute "INSERT INTO new_transaction_log SELECT transaction, user_id, (Year(timestamp) * 10000 + Month(timestamp) * 100 + Day(timestamp)) as date, ((Hour(timestamp) + 1) * 10000 + (Minute(timestamp) + 1)*100 + Second(timestamp) + 1 ) as time, NULL as ns, entity_id from transaction_log";

# check if both have same number of rows
ROWSTEST=$(mysql_execute "SELECT count(*) from new_transaction_log into @newRows; SELECT count(*) from transaction_log into @oldRows; Select (@newRows = @oldRows) as test" | sed -e ':a;N;$!ba;s/[^01]//g') 

if [ 0 -eq "$ROWSTEST" ]; then
 failure "The rows do not match. Something went wrong."
fi

# delete old transaction_log
mysql_execute "DROP TABLE transaction_log;" 

# rename new_transaction_log to transaction_log
mysql_execute "ALTER TABLE new_transaction_log RENAME TO transaction_log" 

success
