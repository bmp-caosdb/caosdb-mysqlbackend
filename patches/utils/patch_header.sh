#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright 2019 Daniel Hornung
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
#header for patch scripts
CMD_MYSQL=mysql
CMD_MYSQL_DUMP=mysqldump
USAGE="$1 [ --env=ENV_FILE ] [ --patch=PATCH ] [ --backupdir=BACKUPDIR ]\n\n"
CMD_OPTIONS=cat <<EOF
options:

-h, --help
    Show brief help.
--env=ENV_FILE
    A file where variables are stored in the format of shell environment
    variables.  Content of this file overrides environment variables already
    present.
--patch=PATCH
    *TODO* Please document this option.
--backupdir=BACKUPDIR
    *TODO* Please document this option.

This script basically uses the same environment variables as the server
configuration make file.  Notable examples are:

- LOGIN_PATH
- MYSQL_HOST
- MYSQL_PORT
- DATABASE_NAME
- DATABASE_USER
- DATABASE_USER_PW

EOF
set -e

function _print_help() {
  echo -e "$USAGE"
  echo -e "$CMD_OPTIONS"
  if [ -n "$PRINT_HELP" ]; then
    echo -e "$PRINT_HELP"
  fi
  if [ -n "$1" ]; then
    echo -e "$1"
  fi
}

while test $# -gt 0; do
 case "$1" in
 -h|--help)
  _print_help
  exit 0
  ;;
 --env*)
  ENV_FILE="${1#--*=}"
  shift
  ;;
 --patch*)
  PATCH="${1#--*=}"
  shift
  ;;
 --backupdir*)
  BACKUPDIR="${1#--*=}"
  echo "Warning: BACKUPDIR is not used currently."
  shift
  ;;
 *)
  echo "Unknown option $1"
  exit 1
  shift
  ;;
 esac
done

[[ -n "$ENV_FILE" ]] && source "$ENV_FILE"

if [[ -z "$DATABASE_NAME" && -z "$MYSQL_CONNECTION" ]]
then
 _print_help "Please specify the database."
 exit 1
fi

if [ "$LOGIN_PATH" ]; then
  MYSQL_CONNECTION="--login-path=$LOGIN_PATH"
  MYSQL_CONNECTION_NO_DB="$MYSQL_CONNECTION"
  MYSQL_CONNECTION="$MYSQL_CONNECTION --database=$DATABASE_NAME"

elif [[ -z "$MYSQL_CONNECTION" ]]; then
  MYSQL_CONNECTION=""
  if [ "$DATABASE_USER" ]
  then
    MYSQL_CONNECTION="--user=$DATABASE_USER"
  fi
  if [ "$DATABASE_USER_PW" ]
  then
    MYSQL_CONNECTION="$MYSQL_CONNECTION --password=$DATABASE_USER_PW"
  fi
  if [[ "$MYSQL_HOST" && ( "$MYSQL_HOST" != "localhost" ) ]]; then
      MYSQL_CONNECTION="$MYSQL_CONNECTION --host=$MYSQL_HOST"
      if [ "$MYSQL_PORT" ]; then
          MYSQL_CONNECTION="$MYSQL_CONNECTION --port=$MYSQL_PORT"
      fi
  fi
  # This option should come last, so we also have one version without the database
  MYSQL_CONNECTION_NO_DB="$MYSQL_CONNECTION"
  MYSQL_CONNECTION="$MYSQL_CONNECTION --database=$DATABASE_NAME"
fi
export MYSQL_CONNECTION
export MYSQL_CONNECTION_NO_DB
export DATABASE_NAME

if [ -n "$PATCH" ]; then
    echo -ne "applying patch $PATCH to $DATABASE_NAME ... "
fi

function success {
    echo "[OK]"
    exit 0
}

function failure {
    echo "[FAILED] $*"
    exit 1
}

function uptodate {
    echo "[UPTODATE]"
    exit 0
}
# @param $1: db version string, e.g. v2.0.0
# @return: 0 on success, 1 on failure
function check_version {
    local version=$($CMD_MYSQL $MYSQL_CONNECTION -B -e "Select CaosDBVersion();")
	  if [[ "$(echo $version | sed 's/^CaosDBVersion()\s//')" = "$1" ]]; then
        return 0
	  fi
    uptodate
}

# @param $1: new version string
function update_version {
    mysql_execute "DROP FUNCTION IF EXISTS CaosDBVersion; CREATE FUNCTION CaosDBVersion() RETURNS VARCHAR(255) DETERMINISTIC RETURN '$1';"
}

function dump_table {
    $CMD_MYSQL_DUMP $MYSQL_CONNECTION_NO_DB $DATABASE_NAME $1 \
                    > ${DATABASE_NAME}.${1}.${OLD_VERSION}.dump.sql
}


function mysql_execute {
	set +e
	$CMD_MYSQL $MYSQL_CONNECTION -e "$1"
	ret=${PIPESTATUS[0]}
	if [ "$ret" -ne 0 ]; then
		failure "MYSQL ERROR"
	fi
	set -e
}

function redo_table {
	  $CMD_MYSQL $MYSQL_CONNECTION < ${DATABASE_NAME}.${1}.${OLD_VERSION}.dump.sql
}

