#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# ADD collection table
# Update mysql schema to version v2.0.29
NEW_VERSION="v2.0.29"
OLD_VERSION="v2.0.28"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*


check_version $OLD_VERSION

mysql_execute 'CREATE TABLE collection_type
    (domain_id INT UNSIGNED NOT NULL, 
	 entity_id INT UNSIGNED NOT NULL, 
	 property_id INT UNSIGNED NOT NULL, 
	 collection VARCHAR(255) NOT NULL,
	 INDEX (`domain_id`, `entity_id`), 
	 INDEX (`entity_id`), 
	 INDEX (`property_id`), 
	 CONSTRAINT `collection_type_domain_id_entity` FOREIGN KEY (`domain_id`) REFERENCES `entities` (`id`),
     CONSTRAINT `collection_type_entity_id_entity` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`),
     CONSTRAINT `collection_type_property_id_entity` FOREIGN KEY (`property_id`) REFERENCES `entities` (`id`));'

update_version $NEW_VERSION

success
