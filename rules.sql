/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

#-- ************
#-- define rules
#-- ************


DELETE FROM rules;

#-- general rules
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'INSERT','CheckPropValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'INSERT','CheckParValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'INSERT','CheckParOblPropPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'INSERT','CheckValueParsable','MUST');

INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'UPDATE','CheckPropValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'UPDATE','CheckParValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'UPDATE','CheckParOblPropPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'UPDATE','CheckValueParsable','MUST');

INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'DELETE','CheckReferenceDependencyExistent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,0,'DELETE','CheckChildDependencyExistent','MUST');


#-- role specific rules
#-- recordtype rules 
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,1,'INSERT','CheckDescPresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,1,'INSERT','CheckNamePresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,1,'INSERT','CheckPropPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,1,'INSERT','SetImpToRecByDefault','MUST');

INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,1,'UPDATE','CheckNamePresent','MUST');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,1,'UPDATE','CheckDescPresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,1,'UPDATE','CheckPropPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,1,'UPDATE','SetImpToRecByDefault','MUST');

#-- record rules
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'INSERT','CheckDescPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'INSERT','CheckNamePresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'INSERT','CheckPropPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'INSERT','CheckParPresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'INSERT','SetImpToFix','MUST');

INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'UPDATE','CheckNamePresent','SHOULD');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'UPDATE','CheckDescPresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'UPDATE','CheckPropPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'UPDATE','CheckParPresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,2,'UPDATE','SetImpToFix','MUST');

#-- file rules
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'INSERT','CheckDescPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'INSERT','CheckNamePresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'INSERT','MatchFileProp','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'INSERT','CheckTargetPathValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'INSERT','SetImpToFix','MUST');

INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'UPDATE','CheckNamePresent','SHOULD');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'UPDATE','CheckDescPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'UPDATE','MatchFileProp','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'UPDATE','CheckTargetPathValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,3,'UPDATE','SetImpToFix','MUST');

#-- property rules
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,4,'INSERT','CheckDatatypePresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,4,'UPDATE','CheckDatatypePresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,4,'INSERT','CheckNamePresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,4,'UPDATE','CheckNamePresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,4,'INSERT','SetImpToFix','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,4,'UPDATE','SetImpToFix','MUST');

#-- query template rules
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,8,'UPDATE','CheckQueryTemplate','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,8,'INSERT','CheckQueryTemplate','MUST');

#-- data type specific rules
#-- reference rules
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,11,'INSERT','CheckRefidPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,11,'INSERT','CheckRefidValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,11,'INSERT','CheckRefidIsaParRefid','SHOULD');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,11,'INSERT','CheckDescPresent','MUST');

INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,11,'UPDATE','CheckRefidPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,11,'UPDATE','CheckRefidValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,11,'UPDATE','CheckRefidIsaParRefid','SHOULD');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,11,'UPDATE','CheckDescPresent','MUST');

#-- integer rules
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,12,'INSERT','CheckUnitPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,12,'INSERT','ParseUnit','SHOULD');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,12,'INSERT','CheckDescPresent','MUST');

#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,12,'UPDATE','CheckDescPresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,12,'UPDATE','CheckUnitPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,12,'UPDATE','ParseUnit','SHOULD');

#-- double rules
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,13,'INSERT','CheckUnitPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,13,'INSERT','ParseUnit','SHOULD');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,13,'INSERT','CheckDescPresent','MUST');

#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,13,'UPDATE','CheckDescPresent','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,13,'UPDATE','CheckUnitPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,13,'UPDATE','ParseUnit','SHOULD');

#-- text rules
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,14,'INSERT','CheckDescPresent','MUST');

#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,14,'UPDATE','CheckDescPresent','MUST');

#-- datetime rules
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,15,'INSERT','CheckDescPresent','MUST');

#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,15,'UPDATE','CheckDescPresent','MUST');

#-- timespan rules
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,16,'INSERT','CheckUnitPresent','SHOULD');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,16,'INSERT','CheckDescPresent','MUST');

INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,16,'UPDATE','CheckUnitPresent','SHOULD');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,16,'UPDATE','CheckDescPresent','MUST');

#-- filereference rules
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,17,'INSERT','CheckRefidValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,17,'INSERT','CheckRefidIsaParRefid','MUST');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,17,'INSERT','CheckDescPresent','MUST');

INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,17,'UPDATE','CheckRefidValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,17,'UPDATE','CheckRefidIsaParRefid','MUST');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,17,'UPDATE','CheckDescPresent','MUST');

#-- SQLite files
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,50,'UPDATE','CheckRefidValid','MUST');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,50,'UPDATE','CheckRefidIsaParRefid','SHOULD');
#-- INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,50,'UPDATE','CheckDescPresent','SHOULD');
INSERT INTO rules (domain_id, entity_id, transaction, criterion, modus) VALUES (0,50,'UPDATE','SQLiteTransaction','MUST');
