# Multipurpose subdomains #
Multipurpose subdomains are generically used to work with composite
properties. Currently implemented examples are:
- Properties with units (if multiple Properties exist)
- Lists
- Name overrides

## Example ##
Let's have a look at this *Record* (simplified XML):

```xml
<R1>
    <P1 name="Comment">Hello World</P1>
    <P2 name="voltage" unit="V">
        23
    </P2>
    <P3 comment="list of something">
        V1, V2, V3, V4, ...
    </P3>
</R1>
```

