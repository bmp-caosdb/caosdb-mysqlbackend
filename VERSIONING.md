# Versioning

Starting with version `TODO: the next version`, the versions of the CaosDB MySQL Back-end must follow the principles of [Semantic Versioning 2.0.0](https://semver.org). In summary:

> Given a version number MAJOR.MINOR.PATCH, increment the:
>
> 1. MAJOR version when you make incompatible API changes,
> 2. MINOR version when you add functionality in a backwards-compatible manner, and
> 3. PATCH version when you make backwards-compatible bug fixes.
>
> Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.


## §1 API Changes.

1. All tables, stored procedures and stored functions which names do not begin with the character `_` belong to the api and are not implementation specific.
2. Specifically, that means for all components of the API:
    1. Changes to the design of existing tables or their colums (name, datatype, etc.) MUST always result in anew MAJOR version. The only exception from this rule are changes to the description of a table or column.
    2. Deleting tables is an incompatible API change and results in a new MAJOR version.
    3. Changes to the stored procedures and functions, which change the signature (i.e the list of parameters) is an incompatible change, and results in a new MAJOR version.
    4. Adding new stored functions, procedures or tables results in a new MINOR version.
    5. Changing just a bit of code inside a stored procedure or function results in a new PATCH version.

## §2 Other Changes.

1. Changes to any non-API tables, functions and procedures results in a new PATCH version.
